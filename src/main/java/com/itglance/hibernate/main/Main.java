package com.itglance.hibernate.main;

import com.itglance.hibernate.entity.Student;
import com.itglance.hibernate.entity.Vehicle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Vehicle vehicle1 = new Vehicle("Car");
        Vehicle vehicle2 = new Vehicle("Truck");

        Student student = new Student("Ramesh");
        Student student2 = new Student("Suresh");

        student.getVehicle().add(vehicle1);
        student2.getVehicle().add(vehicle1);
        student.getVehicle().add(vehicle2);
        student2.getVehicle().add(vehicle2);

        session.save(student);
        session.save(student2);

        transaction.commit();
        session.close();
        sessionFactory.close();
    }

}
